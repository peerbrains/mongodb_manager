﻿namespace ATM.Mongo.Abstraction
{
    public class ErrorMessage
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
