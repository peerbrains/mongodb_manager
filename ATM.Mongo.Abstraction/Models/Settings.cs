﻿namespace ATM.Mongo.Abstraction
{
    public class Settings
    {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
        public string PropertyCollectionName { get; set; }
        public string UserCollectionName { get; set; }
        public string DataProtectionKey { get; set; }
        public string CryptoKeyIV { get; set; }
        public string SubscriberCollectionName { get; set; }
        public string TAQSQueueCollectionName { get; set; }
        public string analyticsConfigDomain { get; set; }
        public string analyticsConfigKey { get; set; }
        public string analyticsUserConfigKey { get; set; }
        public string ContentType { get; set; }
        public string BrokerAPIUrl { get; set; }
       
    }
}
