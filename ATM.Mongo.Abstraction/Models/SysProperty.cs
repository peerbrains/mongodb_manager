﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ATM.Mongo.Abstraction
{
    public class SysProperty: BaseEntity
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public string PropId { get; set; }
        public string HostPropId { get; set; }
        public string PropValue { get; set; }
        public string Description { get; set; }
        public string PropGroup { get; set; }
        public string Host { get; set; }
        public string UserPrefID { get; set; }
        public bool SysCaching { get; set; }
    }
}
