﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ATM.Mongo.Abstraction
{
    public class TAQSQueueEntity : BaseEntity
    {
        //[BsonId]
        public ObjectId Id { get; set; }
        [BsonElement]
        public string Host { get; set; }
        [BsonElement]
        public string ApplicantId { get; set; }
        [BsonElement]
        public int ActivityId { get; set; }
        [BsonElement]
        public string From { get; set; }
        [BsonElement]
        public string To { get; set; }
        [BsonElement]
        public string Message { get; set; }
        [BsonElement]
        public string Status { get; set; }
        [BsonElement]
        public string MessageSID { get; set; }
    }
    public class TAQSQueue :BaseEntity
    {
        public string Host { get; set; }
        public string ApplicantId { get; set; }
        public int ActivityId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
        public string MessageSID { get; set; }
    }
}
