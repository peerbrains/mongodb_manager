﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ATM.Mongo.Abstraction
{
    public class TAQSRegistryEntity : BaseEntity
    {        
        public string UserId { get; set; }
        public string Host { get; set; }
        public string PhoneNumber { get; set; }
		public int PreferredAreaCode { get; set; }
    }

    public class TAQSRegistrywithID : BaseEntity
    {
        //[BsonId]
        public ObjectId Id { get; set; }
        public string UserId { get; set; }
        public string Host { get; set; }
        public string PhoneNumber { get; set; }
        public int PreferredAreaCode { get; set; }
    }
}
