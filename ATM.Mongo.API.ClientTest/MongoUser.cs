﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
namespace ATM.Mongo.API.ClientTest
{
   
    public class MongoUser
    {
        //  public string Id { get; set; }
        public string UserId { get; set; }
        public string Host { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string License { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Mobile { get; set; }
        public string Status { get; set; }
        //public BsonDateTime LastLogin { get; set; }
        //public BsonDateTime LastLogout { get; set; }
        public bool Active { get; set; }
        public DateTime DateCreated { get; set; }
        public long CreatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public long UpdatedBy { get; set; }
    }
}
