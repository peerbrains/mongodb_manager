﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ATM.Mongo.API.ClientTest
{
    [TestClass]
    public class MongoManagerClientTest
    {
        MongoUser mongoUser = null;
        MongoUser emptyMongoUser = null;
        MongoUser editMongoUser = null;
        MongoUser delMongoUser = null;
        MongoUser resetMongoUserPassword = null;
        MongoUser resetMongoUserPassword_WhenRefIdIsNotValid = null;
        string urlEndPoint = ConfigurationManager.AppSettings["ATMMongoURL"].ToString();
        MongoSysProperty mongoSysProperty = null;
        MongoSysProperty mongoSysProperty_Empty = null;
        MongoSysProperty mongoSysProperty_Update = null;
        string mongosysProperty_Delete;
        string mongosysProperty_Delete_invalid, mongoSysProperty_GetPropValue_Valid, mongoSysProperty_GetPropValue_Invalid, mongoSysProperty_GetHostName_Valid, mongoSysProperty_GetHostName_NOtValid;

        public MongoManagerClientTest()
        {
            mongoUser = new MongoUser { UserId="LathaTest",Host="PeerBrains" ,Email="latha@PeerBrains.in", Password="test123", FirstName="Lat", LastName="Val", License="licensing...", Phone="6309998880", CreatedBy=101, Active=true };
            //emptyMongoUser = new MongoUser { };
            editMongoUser = new MongoUser { UserId = "LathaTest",Host="PeerBrains", FirstName = "Test", LastName = "Muthu",UpdatedBy=103 };
            resetMongoUserPassword = new MongoUser { UserId = "LathaTest", Host="PeerBrains", Password = "monk1234" };
            resetMongoUserPassword_WhenRefIdIsNotValid = new MongoUser { UserId = "Lat", Password = "refid000" };
            delMongoUser = new MongoUser
            {
                UserId = "LathaTest",
                Host = "PeerBrains",
                UpdatedBy=103
            };
                mongoSysProperty = new MongoSysProperty { PropId = "test_Page_Size", PropGroup = "Search_Page", SysCaching = false,UserPrefID=101, Description = "For Search Page", Host = "Peerbrains", PropValue = "25",CreatedBy=104,UpdatedBy=104 };
            //mongoSysProperty_Empty = new MongoSysProperty { };
            mongosysProperty_Delete = "Peerbrains:test_Page_Size";
            mongoSysProperty_GetHostName_Valid = "PeerBrains";
            mongoSysProperty_GetHostName_NOtValid = "Peerbrains";
            mongoSysProperty_GetPropValue_Valid = "Peerbrains:test_Page_Size";
            mongosysProperty_Delete_invalid = "";
            mongoSysProperty_GetPropValue_Invalid = "Peerbrains:tst_Page_Size";
            mongoSysProperty_Update = new MongoSysProperty { HostPropId = "Peerbrains:test_Page_Size", SysCaching = true,PropValue = "10", Description = "For the Host PeerBrains" ,UpdatedBy=103};
        }
        
        [TestMethod]
        public void InsertUser_WhenModelIsValid()
        {

            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(mongoUser));
            System.Net.HttpStatusCode result;
            string keyForMongo = "SaveUser";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(mongoUser), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }

            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        

        [TestMethod]
        public void InsertUser_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(emptyMongoUser));
            System.Net.HttpStatusCode result;
            string keyForMongo = "SaveUser";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(emptyMongoUser), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void updatePassword_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(resetMongoUserPassword));
            System.Net.HttpStatusCode result;
            string keyForMongo = "ChangePassword";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(resetMongoUserPassword), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void UpdateUser_WhenModelIsValid()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(editMongoUser));
            System.Net.HttpStatusCode result;
            string keyForMongo = "UpdateUser";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(editMongoUser), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
    
        [TestMethod]
        public void DeleteUser_IsValid()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(delMongoUser));
            System.Net.HttpStatusCode result;
            string keyForMongo = "DeleteUser";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(delMongoUser), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void AddSysProperty_WhenModelIsValid_01()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty));
            System.Net.HttpStatusCode result;
            string keyForMongo = "SaveSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }

            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void AddSysProperty_WhenModelIsEmpty_02()
        {

            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty_Empty));
            System.Net.HttpStatusCode result;
            string keyForMongo = "SaveSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty_Empty), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }

            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void UpdateSysProperty_WhenModelIsValid_03()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty_Update));
            System.Net.HttpStatusCode result;
            string keyForMongo = "UpdateSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty_Update), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void UpdateSysProperty_WhenModelIsEmpty_04()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty_Empty));
            System.Net.HttpStatusCode result;
            string keyForMongo = "UpdateSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(mongoSysProperty_Empty), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        
        [TestMethod]
        public void DeleteSysProperty_WhenHostPropIdIsValid_07()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(mongosysProperty_Delete));
            System.Net.HttpStatusCode result;
            string keyForMongo = "DeleteSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(mongosysProperty_Delete), UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void DeleteSysProperty_WhenHostPropIdIsNotVaild_08()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(mongosysProperty_Delete_invalid));
            System.Net.HttpStatusCode result;
            string keyForMongo = "DeleteSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                StringContent postData = new StringContent(mongosysProperty_Delete_invalid, UnicodeEncoding.UTF8, "application/json");

                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                Trace.WriteLine("The Response Of the Method is :" + response.ToString());
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void GetSysPropertyById_WhenHostPropIdIsNotVaild_06()
        {
            Trace.WriteLine("The Request Object is " + "For hostPropId :"+ mongoSysProperty_GetPropValue_Invalid);
            string keySysProp = "GetSysPropertyById";
            string parameter = "?hostPropId="+ mongoSysProperty_GetPropValue_Invalid;
            string apiUrl = urlEndPoint + keySysProp + parameter;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<MongoSysProperty>(data);
                    Trace.WriteLine("The Response Of the Method is :" + data.ToString());

                }
            }
        }
        [TestMethod]
        public void GetSysPropertyById_WhenHostPropIdIsVaild_05()
        {
            Trace.WriteLine("The Request Object is " + "For hostPropId of :"+ mongoSysProperty_GetPropValue_Valid);
            string keySysProp = "GetSysPropertyById";
            string parameter = "?hostPropId=" + mongoSysProperty_GetPropValue_Valid;
            string apiUrl = urlEndPoint + keySysProp+ parameter;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<MongoSysProperty>(data);
                    Trace.WriteLine("The Object Of hostPropId of PeerBrains:List_Filter is :" + data.ToString());

                }
            }
        }

        [TestMethod]
        public void GetAllPropertiesByHost_WhenHostNameIsVaild_10()
        {
            Trace.WriteLine("The Request Object is " + "For hostName of :"+ mongoSysProperty_GetHostName_Valid);
            string keySysProp = "GetAllPropertiesByHost";
            string parameter = "?hostName=" + mongoSysProperty_GetHostName_Valid;
            string apiUrl = urlEndPoint + keySysProp + parameter;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair>>(data);
                    Trace.WriteLine("The Object Of hostPropId of PeerBrains:List_Filter is :" + data.ToString());

                }
            }
        }
        [TestMethod]
        public void GetAllPropertiesByHost_WhenHostNameIsNotVaild_11()
        {
            Trace.WriteLine("The Request Object is " + "For hostName of :"+ mongoSysProperty_GetHostName_NOtValid);
            string keySysProp = "GetAllPropertiesByHost";
            string parameter = "?hostName=" + mongoSysProperty_GetHostName_NOtValid;
            string apiUrl = urlEndPoint + keySysProp + parameter;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;

                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KeyValuePair>>(data);
                    Trace.WriteLine("The Object Of hostPropId of PeerBrains:List_Filter is :" + data.ToString());

                }
            }
        }
    }
}