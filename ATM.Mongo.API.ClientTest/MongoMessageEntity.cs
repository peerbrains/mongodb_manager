﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Mongo.API.ClientTest
{
    public class MongoMessageEntity
    {
        public string Host { get; set; }
        public int ApplicantId { get; set; }
        public int ActivityId { get; set; }
        public string Sender { get; set; }
        public string Mobile { get; set; }
        public string Msg { get; set; }
        public DateTime DateCreated { get; set; }
        public long CreatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public long UpdatedBy { get; set; }
    }
}
