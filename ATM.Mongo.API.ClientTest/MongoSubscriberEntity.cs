﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Mongo.API.ClientTest
{
    public class MongoSubscriberEntity
    {
        //public ObjectId Id { get; set; }
        public string UserId { get; set; }
        public string Host { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateCreated { get; set; }
        public long CreatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public long UpdatedBy { get; set; }
    }
}
