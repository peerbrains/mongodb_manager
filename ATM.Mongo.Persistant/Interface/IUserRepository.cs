﻿using ATM.Mongo.Abstraction;
using System.Collections.Generic;

namespace ATM.Mongo.Persistant.Interface
{
    public interface IUserRepository
    {
        bool ResetPassword(UserEntity model);
        IEnumerable<UserEntity> GetPasswords();
        UserEntity GetUserById(string userId, string host);
        // UserEntity GetUserByEmailId(string emailId);
        UserEntity GetUserByEmailId(string emailId);
        // UserEntity GetUser(string userId, string password);
        bool SaveUser(UserEntity model);
        bool UpdateUser(UserEntity model);
        bool DeleteUser(UserEntity model);
    }
}
