﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Mongo.Persistant.Interface
{
    public interface ICrypto
    {
        byte[] Encrypt(string input);
        string EncryptToString(string input);
        string Decrypt(byte[] input);
        byte[] DecryptToByte(byte[] input);
        string DecryptString(string input);
    }
}
