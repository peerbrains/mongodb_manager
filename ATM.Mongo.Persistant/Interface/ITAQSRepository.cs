﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ATM.Mongo.Abstraction;

namespace ATM.Mongo.Persistant.Interface
{
    public interface ITAQSRepository
    {
        bool SaveRegistry(List<TAQSRegistryEntity> model);
        bool SaveMessage(List<TAQSQueue> model);
        bool UpdateMessage(TAQSQueueEntity model);
        bool updateSID(TAQSQueueEntity model);
        TAQSQueueEntity GetTAQSQueueByPhoneNumber(string sToPhoneNumber);
        TAQSQueueEntity GetTAQSQueueBySID(string sID);
        Task<List<TAQSRegistrywithID>> GetRegistryByHost(string hostName);
        Task<List<string>> GetRegisteryPhoneNo();
        Task<List<TAQSQueueEntity>> GetQueuedMessages(string queue);
    }
}
