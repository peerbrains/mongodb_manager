﻿using ATM.Mongo.Abstraction;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ATM.Mongo.Persistant.Interface
{
    public interface ISysPropertyRepository
    {
        bool SaveSysProperty(SysProperty model);
        bool UpdateSysProperty(SysProperty model);
        bool DeleteSysProperty(string hostPropId);
        SysProperty GetSysPropertyById(string hostPropId);
        string[] GetUserAnalyticsConfig(string host,string userId,string analyticsSystemHostPropKey,string analyticsUserHostPropKey);        
        Task<List<ATM.Mongo.Abstraction.KeyValuePair>> GetAllPropertiesByHost(string hostName);
    }
}
