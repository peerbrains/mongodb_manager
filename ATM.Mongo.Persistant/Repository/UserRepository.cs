﻿using ATM.Mongo.Abstraction;
using ATM.Mongo.Persistant.Interface;
using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace ATM.Mongo.Persistant.Repository
{
    public class UserRepository : IUserRepository
    {
        private IMongoCollection<UserEntity> Collection { get; }
        private IMongoCollection<TAQSRegistryEntity> _Taqs { get; }
        private IMongoCollection<TAQSQueueEntity> _TaqsQueue { get; }
        private readonly ICrypto _crypto;

        public UserRepository(IMongoCollection<UserEntity> collection, IMongoCollection<TAQSRegistryEntity> Taqs, IMongoCollection<TAQSQueueEntity> TaqsQueue, ICrypto crypto)
        {
            this.Collection = collection;
            this._crypto = crypto;
            this._Taqs = Taqs;
            this._TaqsQueue = TaqsQueue;
        }

        public bool ResetPassword(UserEntity model)
        {
            bool isSaved = false;
            var users = Collection.Find(x => x.UserId.ToLower() == model.UserId.ToLower() && x.Host.ToLower() == model.Host.ToLower() && x.Active == true).FirstOrDefault();
            var userObjectId = users.Id;
            try
            {
                users.Password = _crypto.EncryptToString(model.Password);
                users.DateUpdated = DateTime.Now;
                users.UpdatedBy = model.UpdatedBy;
                Collection.ReplaceOne(Builders<UserEntity>.Filter.Eq("Id", userObjectId), users);
                isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }

        public IEnumerable<UserEntity> GetPasswords()
        {
            try
            {
                return Collection.AsQueryable().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UserEntity GetUserById(string userId, string host)
        {
            try
            {
                return Collection.Find(x => x.UserId.ToLower() == userId.ToLower() && x.Host.ToLower() == host.ToLower()&&x.Active==true).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public UserEntity GetUserByEmailId(string emailId)
        {

            try
            {
                var userEntity = Collection.Find(x => x.Email.ToLower() == emailId.ToLower() && x.Active == true).FirstOrDefault();
                //if (userEntity != null)
                //{
                //    var userPassword = _crypto.DecryptString(userEntity.Password);
                //    //var userPassword = _cipherService.Decrypt(userEntity.Password);
                //    if (userPassword == password)
                //    {
                //        return userEntity;
                //    }
                //}
                return userEntity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveUser(UserEntity model)
        {
            bool isSaved = false;
            try
            {
                model.DateCreated = DateTime.Now;
                model.DateUpdated = DateTime.Now;
                model.Password = _crypto.EncryptToString(model.Password);
                model.Email = model.Email.ToLower();
                model.UserId = model.UserId.ToLower();
                // model.CreatedBy = model.CreatedBy;
                Collection.InsertOne(model);
                isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }

        public bool UpdateUser(UserEntity model)
        {
            bool isSaved = false;
            try
            {
                var users = Collection.Find(x => x.UserId.ToLower() == model.UserId.ToLower() && x.Host.ToLower() == model.Host.ToLower() && x.Active == true).FirstOrDefault();
                var userMongoId = users.Id;
                model.UserId = model.UserId.ToLower();
                users.FirstName = model.FirstName;
                users.LastName = model.LastName;
                users.Email = model.Email.ToLower();
                users.Mobile = model.Mobile;
                users.Extension = model.Extension;
                users.Phone = model.Phone;
                //users.Status = model.Status;
                //users.License = model.License;
                //users.Active = model.Active;
                users.DateUpdated = DateTime.Now;
                users.UpdatedBy = model.UpdatedBy;
                Collection.ReplaceOne(Builders<UserEntity>.Filter.Eq("Id", userMongoId), users);
                isSaved = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }

        public bool DeleteUser(UserEntity model)
        {
            bool isDeleted = false;
            try
            {
                var users = Collection.Find(x => x.UserId.ToLower() == model.UserId.ToLower() && x.Host.ToLower() == model.Host.ToLower() && x.Active == true).FirstOrDefault();
                var userObjectId = users.Id;
                users.Active = false;
                users.DateUpdated = DateTime.Now;
                users.UpdatedBy = model.UpdatedBy;
                Collection.ReplaceOne(Builders<UserEntity>.Filter.Eq("Id", userObjectId), users);
                isDeleted = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isDeleted;
        }
    }
}
