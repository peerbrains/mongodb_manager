﻿using ATM.Mongo.Abstraction;
using ATM.Mongo.Persistant.Interface;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace ATM.Mongo.Persistant.Repository
{
    public class SysPropertyRepository : ISysPropertyRepository
    {
        private IMongoCollection<SysProperty> Collection { get; }      

        public SysPropertyRepository(IMongoCollection<SysProperty> collection)
        {
            Collection = collection;
        }

        public bool SaveSysProperty(SysProperty model)
        {
            bool isSaved = false;
            try
            {

                if (model != null)
                {
                    if (model.PropId != null && model.Host != null)
                    {
                        if (model.HostPropId == null)
                        {
                            model.HostPropId = model.Host + ":" + model.PropId;
                        }
                        model.DateCreated = DateTime.Now;
                        model.DateUpdated = DateTime.Now;
                        model.CreatedBy = model.CreatedBy;
                        model.UpdatedBy = model.UpdatedBy;
                        model.SysCaching = model.SysCaching;
                        Collection.InsertOne(model);
                        isSaved = true;
                    }
                }
                else
                {
                    isSaved = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }

        public bool UpdateSysProperty(SysProperty model)
        {
            bool isSaved = false;
            try
            {
                var sysProperty = Collection.Find(x => x.HostPropId.ToLower() == model.HostPropId.ToLower()).FirstOrDefault();
                var sysPropMongoId = sysProperty.Id;
                if (sysProperty != null)
                {
                    sysProperty.PropValue = model.PropValue;
                    if (model.Description != null || model.Description != "")
                        sysProperty.Description = model.Description;
                    sysProperty.DateUpdated = DateTime.Now;
                    sysProperty.UpdatedBy = model.UpdatedBy;
                    if (model.SysCaching != false)
                        sysProperty.SysCaching = model.SysCaching;
                    
                    Collection.ReplaceOne(Builders<SysProperty>.Filter.Eq("Id", sysPropMongoId), sysProperty);

                    isSaved = true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isSaved;
        }

        public bool DeleteSysProperty(string hostPropId)
        {
            bool isDeleted = false;
            try
            {
                if (!string.IsNullOrEmpty(hostPropId))
                {
                    var sysProperty = Collection.Find(x => x.HostPropId.ToLower() == hostPropId.ToLower()).FirstOrDefault();
                    var userObjectId = sysProperty.Id;
                    if (sysProperty != null)
                    {
                        Collection.DeleteOne(Builders<SysProperty>.Filter.Eq("Id", userObjectId));
                        isDeleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isDeleted;
        }        
        public string[] GetUserAnalyticsConfig(string host,string userId,string analyticsHostPropId,string analyticsUserHostPropId)
        {
            string[] ret = new string[2];
            try
            {
                string globalConfigValue = "";
                //Get the Global system configuration Property Value
                if (analyticsHostPropId != null && analyticsHostPropId != "")
                { 
                    var sProp = Collection.Find(x => x.HostPropId.ToLower() == analyticsHostPropId.ToLower()).FirstOrDefault();
                    globalConfigValue = sProp.PropValue;
                }
                var sUserProp = Collection.Find(x => x.HostPropId.ToLower() == analyticsUserHostPropId.ToLower() && x.UserPrefID.ToLower() == userId.ToLower().Trim()).FirstOrDefault();
                string userConfigValue = "";
                if (sUserProp != null)
                    userConfigValue = sUserProp.PropValue;

                ret[0] = globalConfigValue;
                ret[1] = userConfigValue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }
        public SysProperty GetSysPropertyById(string hostPropId)
        {
            try
            {
                return Collection.Find(x => x.HostPropId.ToLower() == hostPropId.ToLower().Trim()).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<ATM.Mongo.Abstraction.KeyValuePair>> GetAllPropertiesByHost(string hostName)
        {
            List<ATM.Mongo.Abstraction.KeyValuePair> lstProperties = new List<ATM.Mongo.Abstraction.KeyValuePair>();
            try
            {
                var sysProperties = await Collection.Find(x => x.Host.ToLower() == hostName.ToLower()).ToListAsync();
                foreach (var model in sysProperties)
                {
                    lstProperties.Add(new ATM.Mongo.Abstraction.KeyValuePair { Key = model.HostPropId, Value = model.PropValue });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstProperties;
        }
    }
}
