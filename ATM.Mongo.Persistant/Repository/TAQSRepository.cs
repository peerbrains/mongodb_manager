﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ATM.Mongo.Abstraction;
using ATM.Mongo.Persistant.Interface;
using MongoDB.Driver;

namespace ATM.Mongo.Persistant.Repository
{
    public class TAQSRepository : ITAQSRepository
    {
        private IMongoCollection<TAQSRegistryEntity> _Taqs { get; }
        private IMongoCollection<TAQSRegistrywithID> _TaqsWithID { get; }
        private IMongoCollection<TAQSQueueEntity> _TaqsQueueEntity { get; }
        private IMongoCollection<TAQSQueue> _TaqsQueue { get; }

        public TAQSRepository(IMongoCollection<TAQSRegistryEntity> Taqs, IMongoCollection<TAQSRegistrywithID> TaqsWithID, IMongoCollection<TAQSQueueEntity> TAQSQueueEntity, IMongoCollection<TAQSQueue> TaqsQueue)
        {
            this._Taqs = Taqs;
            this._TaqsQueueEntity = TAQSQueueEntity;
            this._TaqsQueue = TaqsQueue;
            this._TaqsWithID = TaqsWithID;
        }
        public bool SaveRegistry(List<TAQSRegistryEntity> model)
        {
            bool isSaved = false;
            try
            {
                foreach (var modelItem in model)
                {
                    modelItem.DateCreated = DateTime.Now;
                    modelItem.DateUpdated = DateTime.Now;
                    modelItem.UserId = modelItem.UserId.ToLower();
                    _Taqs.InsertOne(modelItem);
                }

                isSaved = true;
            }
            catch (Exception ex)
            {
                isSaved = false;
                throw ex;
            }
            return isSaved;
        }

        public bool SaveMessage(List<TAQSQueue> model)
        {
            bool isSaved = false;
            try
            {
                foreach (var modelItem in model)
                {
                    modelItem.DateCreated = DateTime.Now;
                    modelItem.DateUpdated = DateTime.Now;
                    _TaqsQueue.InsertOne(modelItem);
                }

                isSaved = true;
            }
            catch (Exception ex)
            {
                isSaved = false;
                throw ex;
            }
            return isSaved;
        }
        public TAQSQueueEntity GetTAQSQueueBySID(string sID)
        {
            TAQSQueueEntity message = null;
            if (sID != null)
            {
                message = _TaqsQueueEntity.Find(x => x.MessageSID.Equals(sID.Trim())).FirstOrDefault();
                // return message;
            }

            return message;

        }
        public TAQSQueueEntity GetTAQSQueueByPhoneNumber(string sToPhoneNumber)
        {
            TAQSQueueEntity message = null;
            //TAQSRegistrywithID reg = null;
            if (sToPhoneNumber != null)
            {
                message = _TaqsQueueEntity.Find(x => x.To.Equals(sToPhoneNumber.Trim())).FirstOrDefault();                
               // return message;
            }
            //if (message is null)
            //{
            //    reg = _TaqsWithID.Find(x => x.PhoneNumber.Contains(sToPhoneNumber.Trim())).FirstOrDefault();
            //    message = new TAQSQueueEntity();
            //    message.To = reg.PhoneNumber;
            //    message.Host = reg.Host;
            //}

            return message;
            
        }
        
        public bool updateSID(TAQSQueueEntity model)
        {
            bool isSaved = false;
            var message = _TaqsQueueEntity.Find(x => x.ActivityId.Equals(model.ActivityId) && x.Host == model.Host).FirstOrDefault();
            var messageObjectId = message.Id;
            try
            {
                message.DateUpdated = DateTime.Now;
                message.MessageSID = model.MessageSID;
                message.Status = model.Status;
                _TaqsQueueEntity.ReplaceOne(Builders<TAQSQueueEntity>.Filter.Eq("Id", messageObjectId), message);

                isSaved = true;
            }
            catch (Exception ex)
            {
                isSaved = false;
                throw ex;
            }
            return isSaved;
        }

        public bool UpdateMessage(TAQSQueueEntity model)
        {
            bool isSaved = false;
            var message = _TaqsQueueEntity.Find(x =>  x.MessageSID == model.MessageSID).FirstOrDefault();
            var messageObjectId = message.Id;
            try
            {
                message.DateUpdated = DateTime.Now;
                //message.MessageSID = model.MessageSID;
                if (model.Status == "sent" || model.Status == "delivered")
                    message.Status = "SMS SENT";
                else if (model.Status == "queued")
                    message.Status = "TWILIO QUEUED";
                else
                    message.Status = "TWILIO FAILED";
                _TaqsQueueEntity.ReplaceOne(Builders<TAQSQueueEntity>.Filter.Eq("Id", messageObjectId), message);

                isSaved = true;
            }
            catch (Exception ex)
            {
                isSaved = false;
                throw ex;
            }
            return isSaved;
        }
        public async Task<List<TAQSQueueEntity>> GetQueuedMessages(string queue)
        {
            try
            {
                var lstQueuedMessages = await _TaqsQueueEntity.Find(x => x.Status.ToUpper() == queue).ToListAsync();
                return lstQueuedMessages;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<List<TAQSRegistrywithID>> GetRegistryByHost(string hostName)
        {
            try
            {
                var RegistryList = await _TaqsWithID.Find(x => x.Host.ToLower() == hostName.ToLower()).ToListAsync();
                return RegistryList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<string>> GetRegisteryPhoneNo()
        {
            try
            {
                var RegistryList = await _TaqsWithID.Find(x => x.Host.ToLower() != string.Empty ).ToListAsync();
                List<string> registerPhone = new List<string>();
                foreach (var item in RegistryList)
                {
                    var splitPhoneNo = item.PhoneNumber.Substring(item.PhoneNumber.Length - 10); 
                    registerPhone.Add(splitPhoneNo);
                   
                }
                return registerPhone;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
