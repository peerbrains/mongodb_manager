﻿using ATM.Mongo.Persistant.Interface;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace ATM.Mongo.Persistant.Repository
{
    public class Crypto: ICrypto
    {
        string _keyIV = "12345678901234567890123456789012ABCDEFGHIJKLMNOP";
        public Crypto(string key)
        {
            _keyIV = key;
        }

        public byte[] Encrypt(string input)
        {
            byte[] zipText = Encoding.UTF8.GetBytes(input);
            byte[] encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {

                aesAlg.Key = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(0, 32));
                aesAlg.IV = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(32, 16));

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        csEncrypt.Write(zipText, 0, zipText.Length);
                        csEncrypt.FlushFinalBlock();
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return encrypted;
        }

        public string EncryptToString(string input)
        {
            byte[] zipText = Encoding.UTF8.GetBytes(input);
            string encrypted;

            using (AesManaged aesAlg = new AesManaged())
            {

                aesAlg.Key = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(0, 32));
                aesAlg.IV = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(32, 16));

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        csEncrypt.Write(zipText, 0, zipText.Length);
                        csEncrypt.FlushFinalBlock();
                        encrypted = Convert.ToBase64String(msEncrypt.ToArray());
                    }
                }
            }

            return encrypted;
        }

        public string DecryptString(string input)
        {
            string decryptedString = string.Empty;
            byte[] byteInput = Convert.FromBase64String(input);
            decryptedString = Decrypt(byteInput);
            return decryptedString;
        }

        public string Decrypt(byte[] input)
        {
            string zipText = null;

            if (input == null)
            {
                return string.Empty;
            }

            using (AesManaged aesAlg = new AesManaged())
            {

                aesAlg.Key = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(0, 32));
                aesAlg.IV = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(32, 16));

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream())
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Write))
                {
                    csDecrypt.Write(input, 0, input.Length);
                    csDecrypt.FlushFinalBlock();
                    zipText = Encoding.UTF8.GetString(msDecrypt.ToArray());
                    //zipText = Convert.ToBase64String(msDecrypt.ToArray());
                }
            }
            return zipText;
        }

        public byte[] DecryptToByte(byte[] input)
        {
            byte[] zipText = null;

            using (AesManaged aesAlg = new AesManaged())
            {

                aesAlg.Key = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(0, 32));
                aesAlg.IV = System.Text.Encoding.UTF8.GetBytes(_keyIV.Substring(32, 16));

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream())
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Write))
                {
                    csDecrypt.Write(input, 0, input.Length);
                    csDecrypt.FlushFinalBlock();

                    zipText = msDecrypt.ToArray();
                }
            }
            return zipText;
        }

        //public byte[] Zip(string text)
        //{
        //    var bytes = Encoding.UTF8.GetBytes(text);

        //    using (var msi = new MemoryStream(bytes))
        //    using (var mso = new MemoryStream())
        //    {
        //        using (var gs = new GZipStream(mso, CompressionMode.Compress))
        //        {
        //            msi.CopyTo(gs);
        //        }

        //        return mso.ToArray();
        //    }
        //}

        //public string Unzip(byte[] bytes)
        //{
        //    string unzipText = string.Empty;
        //    using (var msi = new MemoryStream(bytes))
        //    using (var mso = new MemoryStream())
        //    {
        //        using (GZipStream gs = new GZipStream(msi, CompressionMode.Decompress))
        //        {
        //            gs.CopyTo(mso);
        //        }

        //        unzipText = Encoding.UTF8.GetString(mso.ToArray());
        //    }

        //    return unzipText;
        //}

        //public byte[] UnzipToByte(byte[] bytes)
        //{
        //    byte[] unZipText = null;

        //    using (var msi = new MemoryStream(bytes))
        //    using (var mso = new MemoryStream())
        //    {
        //        using (GZipStream gs = new GZipStream(msi, CompressionMode.Decompress))
        //        {
        //            gs.CopyTo(mso);
        //        }

        //        unZipText = mso.ToArray();
        //    }

        //    return unZipText;
        //}
       
    }
}
