# Introduction 
ATM.Mongo.API - .NET Core 2 micro service is a connector to Mongo Database from the admin settings portal.

# Getting Started
The End-points exposed are - 
1. GetUserById - GET Endpoint and pass ID and JSON object is returned with all user Details.
2. SaveUser - POST Endpoint with a JSON object as payload and returns true or false whether the record was inserted or not.
3. UpdateUser - POST Endpont with a JSON object as payload and returns true or false whether the record was updated or not. 
4. DeleteUser - POST Endpoint and pass ID and true or false is returned whether the record is deleted or not. The record is just set to active = false in the table. 
5. SaveSysProperty - POST Endpoint and pass JSON object as payload and returns true or false whether the record was inserted or not.
6. UpdateSysProperty - POST Endpoint and pass JSON object as payload and returns true or false whether the record was inserted or not.
7. DeleteSysProperty - POST Endpoint and pass ID and returns true or false whether the record is deleted or not. 
8. GetSysPropertyById - Get Endpoint and pass ID and returns the JSON object of that HostPropID

Config Files - 
ATM.Mongo.API
launchsettings.json - Application URL
nlog.config - Log file paths.
appsettings.json - Mongo ConnectionString and Database reference.
ATM.Mondo.API.ClientTest
app.config - App link.

# Build and Test
1. Postman File is attached to test all the scenarios of the Mongo Connector.
2. Client Test project is added to the Micro Service is also fully functional with test data to test all the scenarios.
3. Error messages where are being passed across Micro Services which are fixed now.
4. Common class file to the error and logging constant strings.
