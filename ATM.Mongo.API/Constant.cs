﻿namespace ATM.Mongo.API
{
    public static class MongoConstant
    {
            public static string APICallStarted = "Execution of API Method {0} Started, {1}";
            public static string APICallEnded = "Execution of API Method {0} Ended, {1}";
            public static string APICallError = "Exception occured in API Method {0}, {1}";
            public static string RepositoryCallStarted = "Execution of Repository Method {0} Started, {1}";
            public static string RepositoryCallEnded = "Execution of Repository Method {0} Ended, {1}";
            public static string RepositoryCallError = "Exception occured in Repository Method {0}, {1}";
    }

}
