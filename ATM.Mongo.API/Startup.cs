﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ATM.Mongo.Persistant.Interface;
using ATM.Mongo.Persistant.Repository;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using MongoDB.Driver;
using Swashbuckle.AspNetCore.Swagger;
using System.Reflection;
using System;
using System.IO;
using ATM.Mongo.Abstraction;

namespace ATM.Mongo.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        private Settings settings { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            settings = configuration.GetSection("Service").Get<Settings>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(option => option.AddPolicy("AllowCors", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));
            services.AddMvc();
            // Repositories setup
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<ISysPropertyRepository, SysPropertyRepository>();
            services.AddTransient<ITAQSRepository, TAQSRepository>();
            services.AddTransient<ICrypto>(p => new Crypto(settings.CryptoKeyIV));
            //Swagger Setup
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "ATM.Mongo API", Version = "v1" });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            //Logging Setup
            services.AddLogging(builder =>
            {
                builder.AddConfiguration(Configuration.GetSection("Logging"))
                       .AddConsole()
                       .AddDebug();
            });
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton(settings);

            //MongoDB Setup
            services.AddSingleton<IMongoClient>(p => new MongoClient(settings.ConnectionString));
            services.AddTransient(p =>
                p.GetRequiredService<IMongoClient>().GetDatabase(settings.Database));
            services.AddTransient(p =>
                p.GetService<IMongoDatabase>().GetCollection<SysProperty>(settings.PropertyCollectionName));
            services.AddTransient(p =>
                p.GetService<IMongoDatabase>().GetCollection<UserEntity>(settings.UserCollectionName));
            services.AddTransient(p =>
                p.GetService<IMongoDatabase>().GetCollection<TAQSRegistryEntity>(settings.SubscriberCollectionName));
            services.AddTransient(p =>
              p.GetService<IMongoDatabase>().GetCollection<TAQSRegistrywithID>(settings.SubscriberCollectionName));
            services.AddTransient(p =>
                p.GetService<IMongoDatabase>().GetCollection<TAQSQueue>(settings.TAQSQueueCollectionName));
            services.AddTransient(p =>
                p.GetService<IMongoDatabase>().GetCollection<TAQSQueueEntity>(settings.TAQSQueueCollectionName));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors("AllowCors");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            
            //Swagger Setup
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mongo API");
            });
            //add NLog to ASP.NET Core
            loggerFactory.AddNLog();

            //.Net Core - NON-ASP.NET
            NLog.LogManager.LoadConfiguration("nlog.config");
            app.UseMvc();
        }
    }
}
