﻿using System;

namespace ATM.Mongo.API.Base
{
    public abstract class BaseEntity
    {
        public DateTime DateCreated { get; set; }
        public string CreatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public string UpdatedBy { get; set; }
    }
}
