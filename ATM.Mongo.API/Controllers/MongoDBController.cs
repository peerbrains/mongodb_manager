﻿using System;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ATM.Mongo.Persistant.Interface;
using System.Reflection;
using System.Collections.Generic;
using System.Threading.Tasks;
using ATM.Mongo.Abstraction;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ATM.Mongo.API.Manager
{
    /// <summary>
    /// Mongo API to handle CRUD operation for main Database connectivity.
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class MongoDBManagerController : Controller
    {
        #region Private Attributes
        private readonly IUserRepository _userRepository;
        private readonly ITAQSRepository _taqsRepository;
        private readonly ISysPropertyRepository _sysPropertyRepository;
        private readonly ILogger _logger;
        public IConfiguration Configuration { get; }
        private Settings settings { get; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="userRepository"></param>
        /// <param name="sysPropertyRepository"></param>
        /// <param name="taqsRepository"></param>
        /// <param name="configuration"></param>
        public MongoDBManagerController(ILogger<MongoDBManagerController> logger, IUserRepository userRepository, ISysPropertyRepository sysPropertyRepository, ITAQSRepository taqsRepository, IConfiguration configuration)
        {
            _userRepository = userRepository;
            _sysPropertyRepository = sysPropertyRepository;
            _logger = logger;
            _taqsRepository = taqsRepository;
            Configuration = configuration;
            settings = configuration.GetSection("Service").Get<Settings>();
        }
        #endregion

        #region API Methods
        /// <summary>
        /// Update password for the user.
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [Route("ChangePassword")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult ChangePassword([FromBody]UserEntity model)
        {
            var isSaved = false;
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            try
            {
                isSaved = _userRepository.ResetPassword(model);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));

                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error inserting the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Gets User information for the user id provided.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="host"></param>
        /// <returns>Retunrs User Model</returns>     
        [HttpGet]
        [Route("GetUserById")]
        [ProducesResponseType(typeof(UserEntity), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetUserById(string userId, string host)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, userId));
            var users = new UserEntity();
            try
            {
                users = _userRepository.GetUserById(userId, host);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, userId));

            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(users);
        }

        /// <summary>
        /// Gets User information for an email address which is provided.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Retunrs User Model</returns>     
        [HttpGet]
        [Route("GetUserByEmail")]
        [ProducesResponseType(typeof(UserEntity), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetUserByEmailId(string email)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, email));
            var user = new UserEntity();
            try
            {
                user = _userRepository.GetUserByEmailId(email);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, email));

            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(user);
        }

        /// <summary>
        /// Saved user information.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns boolean value to indicate successful user - insert.</returns>     
        [HttpPost]
        [Route("SaveUser")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult SaveUser([FromBody]UserEntity model)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            var isSaved = false;
            try
            {
                if (model != null)
                {
                    isSaved = _userRepository.SaveUser(model);
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
                }
                else
                {
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Null Record not allowed." });
                }
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error inserting the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Udpates user value for the provided user model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns boolean value to indicate successful user - update.</returns>     
        [HttpPost]
        [Route("UpdateUser")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult UpdateUser([FromBody]UserEntity model)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            var isSaved = false;
            try
            {
                if (model != null)
                {
                    isSaved = _userRepository.UpdateUser(model);
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
                }
                else
                {
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Null Record not allowed." });
                }
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error Editing the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Delets user value for the provided user id.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns boolean value to indicate successful user - Delete.</returns>    
        [HttpPost]
        [Route("DeleteUser")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult DeleteUser([FromBody]UserEntity model)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model.UserId));
            var isSaved = false;
            try
            {
                if (model != null)
                {
                    isSaved = _userRepository.DeleteUser(model);
                    _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model.UserId));
                }
                else
                {
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Null record not allowed" });
                }
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error Deleting the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Adds Property Details to the SysProperty Table.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns boolean value to indicate successful SysProperty - Insert.</returns>    
        [HttpPost]
        [Route("SaveSysProperty")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult SaveSysProperty([FromBody]SysProperty model)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            var isSaved = false;
            try
            {
                isSaved = _sysPropertyRepository.SaveSysProperty(model);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error inserting the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Updates Property Details to the SysProperty Table.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns boolean value to indicate successful SysProperty - Update.</returns>
        [HttpPost]
        [Route("UpdateSysProperty")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public bool UpdateSysProperty([FromBody]SysProperty model)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            var isSaved = false;
            try
            {
                isSaved = _sysPropertyRepository.UpdateSysProperty(model);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return isSaved;
        }

        /// <summary>
        /// Deletes Property Details to the SysProperty Table.
        /// </summary>
        /// <param name="hostPropId"></param>
        /// <returns>Returns boolean value to indicate successful SysProperty - Delete.</returns>  
        [HttpPost]
        [Route("DeleteSysProperty")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult DeleteSysProperty([FromBody]string hostPropId)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, hostPropId));
            var isSaved = false;
            try
            {
                isSaved = _sysPropertyRepository.DeleteSysProperty(hostPropId);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, hostPropId));
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error Deleting the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Fetches Property Details from the SysProperty Table for the given hostPropID.
        /// </summary>
        /// <param name="hostPropId"></param>
        /// <returns>Returns Json Object of the SysProperty document for the provided hostPropID</returns>    
        [HttpGet]
        [Route("GetSysPropertyById")]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetSysPropertyById(string hostPropId)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, hostPropId));
            var sysProp = new SysProperty();
            try
            {
                sysProp = _sysPropertyRepository.GetSysPropertyById(hostPropId);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, hostPropId));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(sysProp);
        }

        /// <summary>
        /// Fetches User Configuration settings for Analytics portal.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="userId"></param>
        /// <returns>Returns Json User Configuration settings for Analytics portal</returns>    
        [HttpGet]
        [Route("GetUserAnalyticsConfiguration")]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject GetUserAnalyticsConfiguration(string host,string userId)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
            string[] propValue = null;
            try
            {                
                string _sysConfigPropKey = settings.analyticsConfigDomain + ":" + settings.analyticsConfigKey;
                string _userConfigPropKey = host + ":" + settings.analyticsUserConfigKey;
                propValue = _sysPropertyRepository.GetUserAnalyticsConfig(host,userId, _sysConfigPropKey,_userConfigPropKey);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            JObject globalConfig2 = JObject.Parse(propValue[0]);
            dynamic globalConfig = JsonConvert.DeserializeObject(propValue[0]);
            dynamic userConfig;

            if (propValue[1] != null || propValue[1] != "")
            {    
                userConfig = JsonConvert.DeserializeObject(propValue[1]);

                //Loop through each dashboard by comparing the IDs.
                for (int i = 0; i < userConfig.UserConfiguration[0].Dashboards.Count; i++)
                {
                    for (int k = 0; k < globalConfig.Configurations[0].Dashboards.Count; k++)
                    {
                        //check if DatasourceAttribute exists and fetching data through api
                       var DataSourceAttribute = from t in globalConfig2["Configurations"][0]["Dashboards"][k]["Filters"]                                    
                                    select new { ds = (string)t["DataSource"], id = t["Id"] };
                       
                            dynamic DataSourceField = from t1 in DataSourceAttribute
                                                      where t1.ds != null
                                             select new { datasource = t1.ds, Id = t1.id };
                        
                        foreach (var item in DataSourceField)
                        {        //calling the Api to fetch Datasource
                                JArray jConfig = GetListFromBrokerApi(host, userId, item.datasource);

                            if (jConfig != null)
                            {
                                //Check the filterid has DataSource and replace the items with fetchted api items
                                if (globalConfig["Configurations"][0]["Dashboards"][k]["Filters"].Count > 0)
                                {
                                    for (int j = 0; j < globalConfig["Configurations"][0]["Dashboards"][k]["Filters"].Count; j++)
                                    {
                                        if (globalConfig.Configurations[0].Dashboards[k].Filters[j].Id == item.Id)
                                        {
                                            globalConfig.Configurations[0].Dashboards[k].Filters[j].items.Replace(jConfig[0]["Table"]);
                                        }
                                    }
                                }
                            }

                        }
                                
                          //Get the matching Global Dashboard
                         if (globalConfig.Configurations[0].Dashboards[k].Id == userConfig.UserConfiguration[0].Dashboards[i].Id)
                         {
                           //Get the attributes of the dashboards and overwrite the global Attributes
                            globalConfig.Configurations[0].Dashboards[k].DefaultLayout = userConfig.UserConfiguration[0].Dashboards[i].UserDefaultLayout;
                            globalConfig.Configurations[0].Dashboards[k].DefaultPreset = userConfig.UserConfiguration[0].Dashboards[i].UserDefaultPreset;

                            //Get the visibility and display index of element for each dashboard and attach that to the global Config
                            if (userConfig.UserConfiguration[0].Dashboards[i].UserElements.Count > 0)
                            {
                                for (int l = 0; l < globalConfig.Configurations[0].Dashboards[i].Elements.Count; l++)
                                {
                                    for (int j = 0; j < userConfig.UserConfiguration[0].Dashboards[i].UserElements.Count; j++)
                                    {
                                        if (globalConfig.Configurations[0].Dashboards[i].Elements[l].Id == userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].Id)
                                        {
                                            if (userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].Visible != null)
                                            {
                                                globalConfig.Configurations[0].Dashboards[i].Elements[l].Visible = userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].Visible;
                                            }
                                            if (userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].DisplayIndex != null)
                                            {
                                                globalConfig.Configurations[0].Dashboards[i].Elements[l].DisplayIndex = userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].DisplayIndex;
                                            }
                                            //KPIChart Tracker Merge
                                            if (userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].Tracker != null)
                                            {
                                                for (int m= 0; m < userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].Tracker.Count; m++)
                                                {
                                                    if(globalConfig.Configurations[0].Dashboards[i].Elements[l].Tracker[m].Id == userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].Tracker[m].Id)
                                                    {
                                                        globalConfig.Configurations[0].Dashboards[i].Elements[l].Tracker[m].Visible = userConfig.UserConfiguration[0].Dashboards[i].UserElements[j].Tracker[m].Visible;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            //TODO: add default preset (User Specific) if available.
                            //Get the layouts for each dashboard and attach that to the global Config
                            if (userConfig.UserConfiguration[0].Dashboards[i].UserLayouts.Count > 0)
                            {
                                for (int j = 0; j < userConfig.UserConfiguration[0].Dashboards[i].UserLayouts.Count; j++)
                                {
                                    globalConfig.Configurations[0].Dashboards[k].Layouts.Add(userConfig.UserConfiguration[0].Dashboards[i].UserLayouts[j]);
                                }
                            } //End of Layout Merge
                              //Get the Preset filters for each dashboards and add to the Global Filters
                            if (userConfig.UserConfiguration[0].Dashboards[i].UserPresets.Count > 0)
                            {
                                for (int j = 0; j < userConfig.UserConfiguration[0].Dashboards[i].UserPresets.Count; j++)
                                {
                                    globalConfig.Configurations[0].Dashboards[k].Presets.Add(userConfig.UserConfiguration[0].Dashboards[i].UserPresets[j]);
                                }
                            } //End of PreFilter Merge
                            
                            break;
                         }
                    }
                }
                //Get the Elements for each dashboard and attach that to the global config

            }
            return globalConfig;
        }
        /// <summary>
        /// Add Layout/Presets to User Configuration settings for Analytics portal.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="userId"></param>
        /// <param name="userMethod"></param>
        /// <param name="dashboardId"></param>
        /// <returns>Returns Json User Configuration settings for Analytics portal</returns>    
        [HttpPost]
        [Route("AddUserAnalyticsConfiguration")]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject AddUserAnalyticsConfiguration(string host, string userId, string userMethod, string dashboardId)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
            string[] propValue = null;
            dynamic userConfig = null;
            bool isAdd = false;
            JObject globalConfig = null;
            try
            {
                //string _sysConfigPropKey = settings.analyticsConfigDomain + ":" + settings.analyticsConfigKey;
                string _userConfigPropKey = host + ":" + settings.analyticsUserConfigKey;
                propValue = _sysPropertyRepository.GetUserAnalyticsConfig(host, userId, "", _userConfigPropKey);
                bool isSaved = false;
                
                JObject usrAddConfig = JObject.Parse(userMethod);
                var CheckDefault = usrAddConfig.First.Path;

                //If User-Prop doesn't
                if (propValue[1] == null || propValue[1] == "")
                {
                    isAdd = true;
                    string addConfig = "{ 'UserConfiguration': [{'Dashboards': [{'Id': 'D1','UserLayouts': []},[{'UserPresets': []}]]}]}";

                    userConfig = JsonConvert.DeserializeObject(addConfig);
                    userConfig.UserConfiguration[0].Dashboards[0].Id = dashboardId;
                }
                else
                {
                    userConfig = JsonConvert.DeserializeObject(propValue[1]);
                }
                //Get the userConfig File - Check if the DashboardID exists.
                JArray usrDashboards = userConfig["UserConfiguration"][0].Dashboards;
                JObject childDashboards = usrDashboards.Children<JObject>()
                    .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == dashboardId);
                Boolean isNewdashboard = false;
                if (childDashboards == null)
                {
                    isNewdashboard = true;
                    //The tab does not exist for layout or preset. Add the new tab section.
                    string addDashboardConfig = "{'Dashboards': [{'Id': '" + dashboardId + "','UserLayouts': [],'UserPresets': []}]}";

                    dynamic dashboardConfig = JsonConvert.DeserializeObject(addDashboardConfig);
                    JArray uDashConfig = JsonConvert.DeserializeObject(dashboardConfig["Dashboards"].ToString());
                    //userConfig.UserConfiguration[0].Dashboards[0].Id = dashboardId;

                    //usrDashboards.Add(new JObject { { "Id", dashboardId }, { "UserLayouts", "[{}]" },{ "UserPresets", "[]" } });
                    childDashboards = uDashConfig.Children<JObject>()
                     .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == dashboardId);
                }
                if (childDashboards.HasValues)
                {
                    if (usrAddConfig["UserElements"] != null)
                    {
                        //Check if the UserElement ID exists for Delete
                        JObject childUsrElements = childDashboards["UserElements"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == usrAddConfig["UserElements"][0]["Id"].ToString());
                        dynamic childElement = childDashboards;
                        if (childUsrElements != null)
                        { 
                            //Check if visible attribute present and replace with usermethod visible
                            if (childUsrElements["Visible"] != null)
                            {
                                childUsrElements["Visible"] = usrAddConfig["UserElements"][0]["Visible"];
                            }
                            else
                            {
                                childUsrElements.SelectToken("Type").Parent.AddAfterSelf(new JProperty("Visible", usrAddConfig["UserElements"][0]["Visible"]));
                            }
                        }
                        else
                        {
                            //Add the provided to the ChildDashboard
                            childElement.UserElements.Add(usrAddConfig["UserElements"][0]);
                            if (isNewdashboard)
                            {
                                //Add the childlayout to the usrConfig
                                userConfig.UserConfiguration[0].Dashboards.Add(childElement);
                            }
                        }
                    }
                    //Check if the Layout or preset exists
                    if (usrAddConfig["UserLayouts"] != null)
                    {
                        //Check if the Layout ID exists for Add
                        JObject childUsrLayout = childDashboards["UserLayouts"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == usrAddConfig["UserLayouts"][0]["Id"].ToString());
                        dynamic childLayouts = childDashboards;
                        if (childUsrLayout != null)
                        {
                            //Send an error message that the Layout ID already exists?
                            childUsrLayout.Replace(usrAddConfig["UserLayouts"][0]);
                        }
                        else
                        {
                            //Add the provided to the ChildDashboard
                            childLayouts.UserLayouts.Add(usrAddConfig["UserLayouts"][0]);
                            if (isNewdashboard)
                            {
                                //Add the childlayout to the usrConfig
                                userConfig.UserConfiguration[0].Dashboards.Add(childLayouts);
                            }
                        }

                    }
                    //Check if the DefaultLayout exists 
                    if (CheckDefault == "DefaultLayout")
                    {
                        var userdefaultlayoutvalue = usrAddConfig.First.First();
                        if (childDashboards["UserDefaultLayout"] != null)
                        {
                            childDashboards["UserDefaultLayout"] = userdefaultlayoutvalue;
                        }
                        else
                        {
                            childDashboards.SelectToken("UserLayouts").Parent.AddBeforeSelf(new JProperty("UserDefaultLayout", userdefaultlayoutvalue));
                        }
                    }
                    //else
                    //Add the provided to the ChildDashboard
                    //childDashboards.Add(usrAddConfig["UserLayouts"][0]);

                    if (usrAddConfig["UserPresets"] != null)
                    {
                        //Check if preset ID Exists for Add
                        JObject childUsrPreset = childDashboards["UserPresets"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == usrAddConfig["UserPresets"][0]["Id"].ToString());
                        dynamic childLayouts = childDashboards;
                        if (childDashboards == null)
                        {
                            isNewdashboard = true;
                            //The tab does not exist for layout or preset. Add the new tab section.
                            string addDashboardConfig = "{'Dashboards': [{'Id': '" + dashboardId + "','UserLayouts': [],'UserPresets': []}]}";

                            dynamic dashboardConfig = JsonConvert.DeserializeObject(addDashboardConfig);
                            JArray uDashConfig = JsonConvert.DeserializeObject(dashboardConfig["Dashboards"].ToString());
                            //userConfig.UserConfiguration[0].Dashboards[0].Id = dashboardId;

                            childDashboards = uDashConfig.Children<JObject>()
                             .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == dashboardId);
                        }
                        if (childUsrPreset != null)
                            childUsrPreset.Replace(usrAddConfig["UserPresets"][0]);
                        else
                        {
                            childLayouts.UserPresets.Add(usrAddConfig["UserPresets"][0]);
                            //childDashboards.Add(new JProperty("UserPresets1",usrAddConfig["UserPresets"][0]));
                            if (isNewdashboard)
                            {
                                //Add the childlayout to the usrConfig
                                userConfig.UserConfiguration[0].Dashboards.Add(childLayouts);
                            }
                        }


                        //Check if the  Defaultpreset exists and do add or update
                        if (CheckDefault == "DefaultPreset")
                        {
                            var userdefaultpresetvalue = usrAddConfig.First.First();
                            if (childDashboards["UserDefaultPreset"] != null)
                            {
                                childDashboards["UserDefaultPreset"] = userdefaultpresetvalue;
                            }
                            else
                            {
                                childDashboards.SelectToken("UserLayouts").Parent.AddBeforeSelf(new JProperty("UserDefaultPreset", userdefaultpresetvalue));
                            }
                        }

                    }
                        //else                       
                        //    childDashboards.Add(usrAddConfig["UserPresets"][0]);

                        //Save the updated configuration file
                        SysProperty updSysprop = new SysProperty();
                        updSysprop.Host = host;
                        updSysprop.HostPropId = host + ":" + settings.analyticsUserConfigKey;
                        updSysprop.PropValue = userConfig.ToString();
                       updSysprop.CreatedBy = userId;
                       if (isAdd)
                           SaveSysProperty(updSysprop);
                        else
                           UpdateSysProperty(updSysprop);

                         isSaved = true;
                }
                else
                {
                    // No layout available to delete in this dashboard
                    //return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Provided Dashboard is not available for this user/host." });
                    //Call the GetConfigfile to send config json if successfully added.
                    globalConfig = GetUserAnalyticsConfiguration(host, userId);
                    return globalConfig;
                }
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
                if (isSaved)
                {
                    globalConfig = GetUserAnalyticsConfiguration(host, userId);
                    return globalConfig;
                }
                else
                {
                    return globalConfig = JObject.Parse(@"{Status:'OK',Message:'Error deleting the configuration.' }");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                string errorMessage = "{ Status:'OK',Message:'" + ex.Message.Replace("'"," ") + "'}";
                return globalConfig = JObject.Parse(errorMessage);
            }
        }
        /// <summary>
        /// Updates User Configuration settings for Analytics portal.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="userId"></param>
        /// <param name="userMethod"></param>
        /// <param name="dashboardId"></param>
        /// <param name="overRide"></param>
        /// <returns>Returns Json User Configuration settings for Analytics portal</returns>    
        [HttpPost]
        [Route("UpdateUserAnalyticsConfiguration")]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject UpdateUserAnalyticsConfiguration(string host, string userId,string userMethod,string dashboardId, bool overRide = false)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
            string[] propValue = null;
            dynamic userConfig = null;
            JObject globalConfig = null;
            try
            {
                //string _sysConfigPropKey = settings.analyticsConfigDomain + ":" + settings.analyticsConfigKey;
                string _userConfigPropKey = host + ":" + settings.analyticsUserConfigKey;
                propValue = _sysPropertyRepository.GetUserAnalyticsConfig(host, userId, "", _userConfigPropKey);
                bool isSaved = false;
                //If User-Prop doesn't
                if (propValue[1] == null || propValue[1] == "")
                {
                    return globalConfig = JObject.Parse(@"{ Status:'OK', Message:'User Configuration does not exist.' }");

                   // return StatusCode(200, new ErrorMessage { Status = "OK", Message = "User Configuration does not exist" });
                }
                else
                {
                    userConfig = JsonConvert.DeserializeObject(propValue[1]);
                }
                //Get the userConfig File - Check if the DashboardID exists.
                JArray usrDashboards = userConfig["UserConfiguration"][0].Dashboards;
                JObject childDashboards = usrDashboards.Children<JObject>()
                    .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == dashboardId);
                JObject usrupdConfig = JObject.Parse(userMethod);
                //usrupdConfig["OVerRide"].ToString() == "False";
                var CheckingDefault = usrupdConfig.First.Path;

                if (childDashboards.HasValues)
                {
                    //Check if Element exists and overRide the current Element for KPI chart
                   if (usrupdConfig["OverRide"] != null && usrupdConfig["OverRide"].ToString() == "True"  && usrupdConfig["UserElements"] != null)
                    {
                        JObject childUsrElement = childDashboards["UserElements"].Children<JObject>()
                              .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == usrupdConfig["UserElements"][0]["Id"].ToString());
                        dynamic childElement = childDashboards;
                        if (childUsrElement != null)
                        {
                            childUsrElement.Replace(usrupdConfig["UserElements"][0]);
                        }
                        else
                        {
                            //Add the provided to the ChildDashboard
                            childElement.UserElements.Add(usrupdConfig["UserElements"][0]);
                        }
                   }
                    
                    else if (usrupdConfig["UserElements"] != null && overRide == false) 
                    {
                        for (int j = 0; j < usrupdConfig["UserElements"].Count(); j++)
                        {
                            //Check if the Element ID exists for Update
                            JObject childUsrElement = childDashboards["UserElements"].Children<JObject>()
                              .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == usrupdConfig["UserElements"][j]["Id"].ToString());
                            dynamic childElement = childDashboards;
                            if (childUsrElement != null)
                            {
                                if (childUsrElement["DisplayIndex"] != null)
                                {
                                    childUsrElement["DisplayIndex"] = usrupdConfig["UserElements"][j]["DisplayIndex"];
                                }
                                else
                                {
                                    childUsrElement.SelectToken("Type").Parent.AddAfterSelf(new JProperty("DisplayIndex", usrupdConfig["UserElements"][j]["DisplayIndex"]));
                                }
                            }
                            else
                            {
                                //Add the provided to the ChildDashboard
                                childElement.UserElements.Add(usrupdConfig["UserElements"][j]);

                            }

                        }
                        
                        
                    }
                    
                    //Check if the Layout or preset exists
                    if (usrupdConfig["UserLayouts"] != null)
                    {
                        //Check if the Layout ID exists for Update
                        JObject childUsrLayout = childDashboards["UserLayouts"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == usrupdConfig["UserLayouts"][0]["Id"].ToString());

                            if (childUsrLayout != null)
                                childUsrLayout.Replace(usrupdConfig["UserLayouts"][0]);
                    }
                    //Check if the DefaultLayout  exists and Updating 
                    if (CheckingDefault == "DefaultLayout")
                    {
                        if (childDashboards["UserDefaultLayout"] != null)
                        {
                            var DefaultLayoutValue = usrupdConfig.First.First();

                            childDashboards["UserDefaultLayout"] = DefaultLayoutValue;
                        }
                    }

                    if (usrupdConfig["UserPresets"] != null)
                    {
                        //Check if preset ID Exists for Udpate
                        JObject childUsrPreset = childDashboards["UserPresets"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == usrupdConfig["UserPresets"][0]["Id"].ToString());
                        if (childUsrPreset != null)
                            childUsrPreset.Replace(usrupdConfig["UserPresets"][0]);
                    }
                    //Check if the  Defaultpreset exists and Updating 
                    if (CheckingDefault == "DefaultPreset")
                    {
                        if (childDashboards["UserDefaultPreset"] != null)
                        {
                            var DefaultPresetValue = usrupdConfig.First.First();

                            childDashboards["UserDefaultPreset"] = DefaultPresetValue;
                        }
                    }

                    //Save the updated configuration file
                    SysProperty updSysprop = new SysProperty();
                    updSysprop.Host = host;
                    updSysprop.HostPropId = host + ":" + settings.analyticsUserConfigKey;
                    updSysprop.PropValue = userConfig.ToString();
                    updSysprop.UpdatedBy = userId;

                    UpdateSysProperty(updSysprop);
                    isSaved = true;

                }
                else
                {
                    // No layout available to update in this dashboard
                    //return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Provided Dashboard is not available for this user/host." });
                    return globalConfig = JObject.Parse(@"{Status:'OK', Message:'Provided Dashboard is not available for this user/host.' }");
                }

                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host + ":" + userId));

                if (isSaved)
                {
                    globalConfig = GetUserAnalyticsConfiguration(host, userId);
                    return globalConfig;

                    //return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                }
                else
                    return globalConfig = JObject.Parse(@"{ Status:'OK', Message:'Error updating the configuration.' }");

            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                string errMessage = "{Status:'Error', Message:'"+ ex.Message + ".' }";
                return globalConfig = JObject.Parse(errMessage);
                //return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }
        /// <summary>
        /// Delete User Configuration settings for Analytics portal.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="userId"></param>
        /// <param name="dashboardId"></param>
        /// <param name="element"></param>
        /// <param name="deleteId"></param>
        /// <returns>Returns Json User Configuration settings for Analytics portal</returns>    
        [HttpPost]
        [Route("DeleteUserAnalyticsConfiguration")]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject DeleteUserAnalyticsConfiguration(string host, string userId, string dashboardId,string element,string deleteId)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
            string[] propValue = null;
            dynamic userConfig = null;
            JObject globalConfig = null;
            try
            {
                //string _sysConfigPropKey = settings.analyticsConfigDomain + ":" + settings.analyticsConfigKey;
                string _userConfigPropKey = host + ":" + settings.analyticsUserConfigKey;
                propValue = _sysPropertyRepository.GetUserAnalyticsConfig(host, userId, "", _userConfigPropKey);
                bool isSaved = false;
                //If User-Prop doesn't
                if (propValue[1] == null || propValue[1] == "")
                {
                    return globalConfig = JObject.Parse(@"{ Status:'OK', Message:'User Configuration does not exist.' }");

                 //   return StatusCode(200, new ErrorMessage { Status = "OK", Message = "User Configuration does not exist" });
                }
                else
                {
                    userConfig = JsonConvert.DeserializeObject(propValue[1]);
                }
                //Get the userConfig File - Check if the DashboardID exists.
                JArray usrDashboards = userConfig["UserConfiguration"][0].Dashboards;
                JObject childDashboards = usrDashboards.Children<JObject>()
                    .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == dashboardId);
                if (childDashboards.HasValues)
                {
                    
                    //Check if the element type is layout / preset
                    if (element == "LAYOUT")
                    {
                        //Check if the Layout ID exists for Delete
                        JObject childUsrLayout = childDashboards["UserLayouts"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == deleteId);
                        if (childUsrLayout != null)
                        {
                            JArray cUserLayouts = (JArray)childDashboards["UserLayouts"];
                            cUserLayouts.Remove(childUsrLayout);
                            //childUsrLayout.RemoveAll();
                        }
                    }
                    else if (element == "PRESET")
                    {
                        //Check if preset ID Exists for delete
                        JObject childUsrPreset = childDashboards["UserPresets"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == deleteId);
                        if (childUsrPreset != null)
                        {
                            JArray cUserPresets = (JArray)childDashboards["UserPresets"];
                            cUserPresets.Remove(childUsrPreset);
                        }
                    }
                    else if (element == "ELEMENT")
                    {
                        //Check if preset ID Exists for delete
                        JObject childUsrElement = childDashboards["UserElements"].Children<JObject>()
                            .FirstOrDefault(o => o["Id"] != null && o["Id"].ToString() == deleteId);
                        if (childUsrElement != null)
                        {
                            childUsrElement["Visible"] = "false";
                        }
                        //childUsrPreset;

                    }
                    else
                    {
                        // Element name not available
                        return globalConfig = JObject.Parse(@"{Status:'OK', Message:'Element name not available.' }");

                       // return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Element name not available" });
                    }
                    //Save the updated configuration file
                    SysProperty updSysprop = new SysProperty();
                    updSysprop.Host = host;
                    updSysprop.HostPropId = host + ":" + settings.analyticsUserConfigKey;
                    updSysprop.PropValue = userConfig.ToString();
                    updSysprop.UpdatedBy = userId;

                    UpdateSysProperty(updSysprop);
                    isSaved = true;
                    globalConfig = GetUserAnalyticsConfiguration(host, userId);
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host + ":" + userId));

                    return globalConfig;
                }
                else
                {
                    // No layout available to delete in this dashboard
                    //return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Provided Dashboard is not available for this user/host." });
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
                    return globalConfig = JObject.Parse(@"{ Status:'OK', Message:'Provided Dashboard is not available for this user/host.' }");

                }

                //if (isSaved)
                //    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                //else
                //    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error deleting the configuration." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                string errMessage = "{ Status:'Error', Message:'" + ex.Message + "'}";
                return globalConfig = JObject.Parse(errMessage);
                //return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }
        /// <summary>
        /// Fetches Property Details from the SysProperty Table for the given hostName.
        /// </summary>
        /// <param name="hostName"></param>
        /// <returns>Returns List of Properties for provided hostName</returns>    
        [HttpGet]
        [Route("GetAllPropertiesByHost")]
        [ProducesResponseType(typeof(IList<ATM.Mongo.Abstraction.KeyValuePair>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public async Task<JsonResult> GetAllPropertiesByHost(string hostName)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, hostName));
            var sysProperties = new List<ATM.Mongo.Abstraction.KeyValuePair>();
            try
            {
                sysProperties = await _sysPropertyRepository.GetAllPropertiesByHost(hostName);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, hostName));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(sysProperties);
        }
        /// <summary>
        /// Fetches Message Queue from the TAQSQueue from the Message ID.
        /// </summary>
        /// <param name="sID"></param>
        /// <returns>Returns Json Object of the MessageQueue</returns>    
        [HttpGet]
        [Route("GetTAQSQueueBySID")]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetTAQSQueueBySID(string sID)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, sID));
            TAQSQueueEntity taqsQueue = new TAQSQueueEntity();
            try
            {

                taqsQueue = _taqsRepository.GetTAQSQueueBySID(sID);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, sID));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(taqsQueue);
        }

        /// <summary>
        /// Fetches Message Details from the given To Phone Number.
        /// </summary>
        /// <param name="sToPhoneNumber"></param>
        /// <returns>Returns Json Object of the MessageQueue </returns>    
        [HttpGet]
        [Route("GetTAQSQueueByPhoneNumber")]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetTAQSQueueByPhoneNumber(string sToPhoneNumber)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, sToPhoneNumber));
            TAQSQueueEntity taqsQueue = new TAQSQueueEntity();
            try
            {

                taqsQueue = _taqsRepository.GetTAQSQueueByPhoneNumber(sToPhoneNumber);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, sToPhoneNumber));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(taqsQueue);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="registry"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveRegistry")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult SaveRegistry([FromBody]List<TAQSRegistryEntity> registry)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, registry));
            var isSaved = false;
            try
            {
                if (registry != null && registry.Count > 0)
                {
                    isSaved = _taqsRepository.SaveRegistry(registry);
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, registry));
                }
                else
                {
                    return StatusCode(200, new ErrorMessage { Status ="OK", Message = "Null Record not allowed." });
                }
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error inserting the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }

        }

        /// <summary>
        /// Saves the message n Queue in Mongo TAQSQueue Collection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveMessage")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult SaveMessage([FromBody]List<TAQSQueue> request)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, request));
            var isSaved = false;
            try
            {
                if (request != null && request.Count > 0)
                {
                    isSaved = _taqsRepository.SaveMessage(request);
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, request));
                }
                else
                {
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Null Record not allowed." });
                }
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error inserting the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateMessage")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult updateMessage([FromBody]TAQSQueueEntity message)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, message));
            var isSaved = false;
            try
            {
                if (message != null)
                {
                    isSaved = _taqsRepository.UpdateMessage(message);
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, message));
                }
                else
                {
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Null Record not allowed." });
                }
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error Updating the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("updatesID")]
        [ProducesResponseType(typeof(ObjectResult), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult updatesID([FromBody]TAQSQueueEntity message)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, message));
            var isSaved = false;
            try
            {
                if (message != null)
                {
                    isSaved = _taqsRepository.updateSID(message);
                    _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, message));
                }
                else
                {
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Null Record not allowed." });
                }
                if (isSaved)
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = isSaved.ToString() });
                else
                    return StatusCode(200, new ErrorMessage { Status = "OK", Message = "Error Updating the data." });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRegistryByHost")]
        [ProducesResponseType(typeof(List<TAQSRegistrywithID>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public async Task<JsonResult> GetRegistryByHost(string hostName)
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, hostName));
            var registryList = new List<TAQSRegistrywithID>();
            try
            {
                registryList = await _taqsRepository.GetRegistryByHost(hostName);
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, hostName));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(registryList);
        }
        /// <summary>
        /// Get list of queued messages for specified host.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetQueuedMessages")]
        [ProducesResponseType(typeof(List<TAQSQueue>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public async Task<JsonResult> GetQueuedMessages(string queue)
        {
            //_logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name));
            List<TAQSQueueEntity> lstQueuedMessages = new List<TAQSQueueEntity>();
            try
            {
                lstQueuedMessages = await _taqsRepository.GetQueuedMessages(queue);
                //_logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(lstQueuedMessages);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param ></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetRegistryPhoneNo")]
        [ProducesResponseType(typeof(List<String>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public async Task<List<string>> GetRegistryPhoneNo()
        {
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, "GetRegistryPhoneNo"));
            var registryList = new List<string>();
            try
            {
                registryList = await _taqsRepository.GetRegisteryPhoneNo();
                _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, "GetRegistryPhoneNo"));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return registryList;
        }
        private JArray GetListFromBrokerApi(string host, string userId, string url)
        {
            JArray response = null;
            _logger.LogInformation(String.Format(MongoConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(settings.BrokerAPIUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(settings.ContentType));
                    StringContent content = new StringContent(JsonConvert.SerializeObject(host), System.Text.Encoding.UTF8, settings.ContentType);
                    var res = client.GetAsync( "/api"+ url + "?"+ "host=" + host + "&userId=" + userId).Result;
                    if (res.IsSuccessStatusCode)
                    {
                       _logger.LogInformation(String.Format(MongoConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host + ":" + userId));
                        return response= JsonConvert.DeserializeObject<JArray>(res.Content.ReadAsStringAsync().Result.ToString());
                    }
                } 
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(MongoConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                string errMessage = "{Status:'Error',Message:'" + ex.Message + "'}";
                return JArray.Parse(errMessage);
            }
            return response;
        }
        #endregion

    }
}
